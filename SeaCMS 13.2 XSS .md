# Introduction

SeaCMS 13.3 has a cross-site scripting (XSS) vulnerability. The root cause of this vulnerability is that some user input is not properly filtered and encoded, allowing malicious users to inject and execute malicious JavaScript code. Attackers can bypass input validation and output escape mechanisms by carefully constructing malicious scripts, inject malicious code into the page, and then execute it in the victim's browser.

# Environment

https://www.seacms.com/download/%E6%B5%B7%E6%B4%8BCMS_%E5%AE%89%E8%A3%85%E5%8C%85_v13.3.zip

# Analysis

![QQ截图20240703085026-1](./QQ截图20250212221045-xss-1.png)

In the file /js/player/dmplayer/player/index.php, the server reads the parameter next in the Get request and outputs it to the page without filtering, allowing attackers to implement XSS attacks through closed tags.

![img](./sink-1.png)

![sink-2](./sink-2.png)

# Verify

POC：

```
url/js/player/dmplayer/player/index.php?next="</script><script>alert('xss')</script>"&nextdz=1
```

After entering POC, a pop-up window popped up successfully on the web page

![img](./QQ截图20250212221521-xss-2.png)
